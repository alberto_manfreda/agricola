/***********************************************************************
Copyright (C) 2018 Alberto Manfreda

For the license terms see the file LICENSE, distributed along with this
software.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
***********************************************************************/

#include <iostream>
#include <algorithm>


#include "game/include/engine.h"


/*
  Basic constructor
 */
GameEngine::GameEngine(QObject *parent)
{
  if (parent != nullptr) {
    parent = nullptr;
  }
  buildStateMachine();
}


/*
  Build the state machine
 */
void GameEngine::buildStateMachine()
{
  // Initialize the state machine
  _phaseMachine = new QStateMachine(this);
  
  /* Now add all the phases to the state machine */
  
  // The preparation phase
  QState *preparationPhase = new QState(_phaseMachine);
  preparationPhase->assignProperty(this, "phase", Phase::PREPARATION);
  _phaseMachine->addState(preparationPhase);

  // The work phase  
  QState *workPhase = new QState(_phaseMachine);
  workPhase->assignProperty(this, "phase", Phase::WORK);
  _phaseMachine->addState(workPhase);

  // The return home phase  
  QState *returnHomePhase = new QState(_phaseMachine);
  returnHomePhase->assignProperty(this, "phase", Phase::RETURN_HOME);
  _phaseMachine->addState(returnHomePhase);

  // The harvest - fields phase  
  QState *harvestFieldsPhase = new QState(_phaseMachine);
  harvestFieldsPhase->assignProperty(this, "phase", Phase::HARV_FIELD);
  _phaseMachine->addState(harvestFieldsPhase);

  // The harvest - feeding phase  
  QState *harvestFeedingPhase = new QState(_phaseMachine);
  harvestFeedingPhase->assignProperty(this, "phase", Phase::HARV_FEED);
  _phaseMachine->addState(harvestFeedingPhase);
  
  // The harvest - breeding phase
  QState *harvestBreedingPhase = new QState(_phaseMachine);
  harvestBreedingPhase->assignProperty(this, "phase", Phase::HARV_BREED);
  _phaseMachine->addState(harvestBreedingPhase);

  // The inital phase is the preparation phase
  _phaseMachine->setInitialState(preparationPhase);
}


/*
  Switch the phase and notify it
 */
void GameEngine::setPhase(Phase phase)
{
  _phase = phase;
  emit phaseChanged(_phase);
}


/*
  Add a new player to the match
 */
void GameEngine::addPlayer(PlayerAttr::Color playerColor,
                           const std::string& playerName)
{
  for (const auto& player: _players) {
    if (player->color() == playerColor) {
      std::cout << "The color " << playerColor << " is already taken! "
                << "Please select a diffeent one." << std::endl;
      return;
    }
  }
  _players.push_back(
    std::unique_ptr<Player>(new Player(playerColor, playerName)));
}


/*
  Run the match
 */
void GameEngine::run()
{
  std::cout << "There are " << _players.size() << " players:" << std::endl;
  for (const auto& player: _players) {
    std::cout << (*player);
  }
  std::cout << std::endl;
  
  loadGameBoard(_players.size());
  int currentRound = 1;
  const int numOfRounds = 14;
  _phaseMachine->start();
  while (currentRound <= numOfRounds) {
    playRound(currentRound);
    currentRound++;
  }
  std::cout << "Game end" << std::endl;
}


/*
  Load the game board appropriate for the number of players
 */
void GameEngine::loadGameBoard(size_t numPlayers)
{
  std::cout << "Loading the game board for " << numPlayers << " players..."
            << std::endl;
  return;
}


/*
  Play one round of the game
 */
void GameEngine::playRound(int roundNo)
{
  std::cout << "We are playing round n. " << roundNo << std::endl;
  while(!actionsEnded()){
      queryActionFromPlayer(nextPlayer());
  }
  if (isHarvestRound(roundNo)) {
    playHarvest();
  }
  for (auto& player : _players) {
    player->returnHome();
  }  
  return;
}


/*
  Wait for the next player to act
 */
void GameEngine::queryActionFromPlayer(Player& player) const
{
  std:: cout << player.name() << " is now playing" << std::endl;
  player.takeAction();
}


/*
 Play the Harvest
 */
void GameEngine::playHarvest()
{
  std:: cout << "Harvest!" << std::endl;
}



/*
 Check whether players have finished the actions available
 */
bool GameEngine::actionsEnded() const
{
  for (const auto& player : _players) {
    if (player->hasActions()) {
      return false;
    }
  }
  return true;
}


/*
 Check whether there is a harvest this round
 */
bool GameEngine::isHarvestRound(int roundNo) const
{
  static const std::vector<int> harvestRounds = {4, 7, 9, 11, 13, 14};
  return (std::find(harvestRounds.begin(), harvestRounds.end(), roundNo) !=\
          harvestRounds.end());
}


Player& GameEngine::nextPlayer() const
{
  static const int maxPlayerNo = _players.size() -1;
  static int nextPlayerNo = -1;
  if (nextPlayerNo < maxPlayerNo) {
    ++nextPlayerNo;
  }
  else {
    nextPlayerNo = 0;
  }
  return *(_players.at(nextPlayerNo));
}
