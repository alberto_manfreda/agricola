/***********************************************************************
Copyright (C) 2018 Alberto Manfreda

For the license terms see the file LICENSE, distributed along with this
software.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
***********************************************************************/

#include "game/include/goods.h"


namespace Goods {

  const std::string resourceName(const Resource& resource)
  {
    switch (resource)
    {
      case (WOOD):
      {
        return "Wood";
      }
      case (CLAY):
      {
        return "Clay";
      }
      case (REED):
      {
        return "Reed";
      }
      case (STONE):
      {
        return "Stone";
      }
      case (FOOD):
      {
        return "Food";
      }
      case (GRAIN):
      {
        return "Grain";
      }
      case (VEGETABLE):
      {
        return "Vegetable";
      }
      case (FENCE):
      {
        return "Fence";
      }
      default:
      {
        return "Unkown";
      }
    }
  }


  const std::string animalName(const Animal& animal)
  {
    switch (animal)
    {
      case (SHEEP):
      {
        return "Sheep";
      }
      case (BOAR):
      {
        return "Boar";
      }
      case (CATTLE):
      {
        return "Cattle";
      }
      default:
      {
        return "Unkown";
      }
    }
  }
  
}
