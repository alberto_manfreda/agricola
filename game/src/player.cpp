/***********************************************************************
Copyright (C) 2018 Alberto Manfreda

For the license terms see the file LICENSE, distributed along with this
software.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
***********************************************************************/

#include "game/include/player.h"


namespace PlayerAttr
{
  std::string colorString(Color color)
  {
    switch (color)
    {
      case (GREEN):
      {
        return "Green";
      }
      case (RED):
      {
        return "Red";
      }
      case (BLUE):
      {
        return "Blue";
      }
      case (PURPLE):
      {
        return "Purple";
      }
      case (WHITE):
      {
        return "White";
      }
      default:
      {
        return "Unkown";
      }
    }
  }
}


const std::string& Player::name() const
{
  return _name;
}


PlayerAttr::Color Player::color() const
{
  return _color;
}


int Player::foodAmount() const
{
 return _foodAmount;
}


int Player::woodAmount() const
{
  return _woodAmount;
}


int Player::reedAmount() const
{
  return _reedAmount;
}


int Player::clayAmount() const
{
  return _clayAmount;
}


int Player::stoneAmount() const
{
  return _stoneAmount;
}


int Player::numFamilyMembers() const
{
  return 2;
}


bool Player::hasActions() const
{
  return (_homeFamilyMembers > 0);
}


void Player::takeAction()
{
  _homeFamilyMembers--;
  _occupiedFamilyMembers++;
}


void Player::returnHome()
{
  _homeFamilyMembers += _occupiedFamilyMembers;
  _occupiedFamilyMembers = 0;
}

// Overloaded << operator.
std::ostream& operator<<(std::ostream& os, const Player& p)
{
  return p.fillStream(os);
}


// Streamer function for overloading the << operator.
std::ostream& Player::fillStream(std::ostream& os) const
{
  os << name() << " is the " << PlayerAttr::colorString(color())
     << " player." << std::endl;
  return os;
}
