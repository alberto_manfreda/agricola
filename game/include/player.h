/***********************************************************************
Copyright (C) 2018 Alberto Manfreda

For the license terms see the file LICENSE, distributed along with this
software.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
***********************************************************************/


#ifndef PLAYER_H
#define PLAYER_H


#include <string>
#include <iostream>
#include <iomanip>

#include "game/include/goods.h"


namespace PlayerAttr
{
  enum Color
  {
    GREEN,
    RED,
    BLUE,
    PURPLE,
    WHITE
  };
  
  std::string colorString(Color color);
};


class Player
{
 
 public:
  
  // Constructor
  Player(PlayerAttr::Color color, const std::string& name) :
    _color(color),
    _name(name)
    {;}
  
  // Getters
  const std::string& name() const;
  PlayerAttr::Color color() const;
  int foodAmount() const;
  int woodAmount() const;
  int reedAmount() const;
  int clayAmount() const;
  int stoneAmount() const;
  int numFamilyMembers() const;
  bool hasActions() const;

  void takeAction();
  void returnHome();
  
  // Overloaded << operator.
  friend std::ostream& operator<<(std::ostream& os, const Player& p);

  
 private:
   
   PlayerAttr::Color _color;
   std::string _name;
   int _foodAmount  = 0;
   int _woodAmount = 0;
   int _reedAmount = 0;
   int _clayAmount = 0;
   int _stoneAmount = 0;
   int _homeFamilyMembers = 2;
   int _occupiedFamilyMembers = 0;
   
  // Streamer function for overloading the << operator.
  std::ostream& fillStream(std::ostream& os) const;

};


#endif //PLAYER_H
