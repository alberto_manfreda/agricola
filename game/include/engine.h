/***********************************************************************
Copyright (C) 2018 Alberto Manfreda

For the license terms see the file LICENSE, distributed along with this
software.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
***********************************************************************/


#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include <memory>
#include <vector>

#include <QObject>
#include <QState>
#include <QStateMachine>

#include "game/include/player.h"


class GameEngine: public QObject
{
 
 Q_OBJECT
 Q_PROPERTY(Phase phase READ phase WRITE setPhase NOTIFY phaseChanged)
  
 public:
  
  // Enum for the phases of the game
  enum Phase {
    PREPARATION,
    WORK,
    RETURN_HOME,
    HARV_FIELD,
    HARV_FEED,
    HARV_BREED
  };
  // The enumeration type must be registered with the Meta-Object System using
  // the Q_ENUM() macro. Registering an enumeration type makes the enumerator
  // names available for use in calls to QObject::setProperty(). 
  Q_ENUM(Phase);
  
  // Constructor
  explicit GameEngine(QObject *parent=nullptr);
  
  // READ getter for the Q_PROPERTY (read the phase of the game)
  Phase phase() const {return _phase;}
  // WRITE setter for the Q_PROPERTY (set the phase of the game)
  void setPhase(Phase phase);
  
  // Add a player to the game
  void addPlayer(PlayerAttr::Color playerColor, const std::string& playerName);
  
  // Run the game engine
  void run();
  
  signals:
    void phaseChanged(Phase);
  
 private:
 
  /****** Data members  ******/
  // A vector of pointers to the players currently in the game
  std::vector<std::unique_ptr<Player>> _players;
  // The current phase of the game
  Phase _phase;
  /* A state machine for handling the switch bewtween different phases of the
   game */
  QStateMachine *_phaseMachine;
  
  /****** Member functions ******/
  void buildStateMachine();
  void loadGameBoard(size_t numPlayers);
  void playRound(int roundNo);
  void queryActionFromPlayer(Player& player) const;
  void playHarvest();
  bool actionsEnded() const;
  bool isHarvestRound(int roundNo) const;
  Player& nextPlayer() const;

};


#endif //GAMEENGINE_H
