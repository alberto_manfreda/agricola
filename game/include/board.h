/***********************************************************************
Copyright (C) 2018 Alberto Manfreda

For the license terms see the file LICENSE, distributed along with this
software.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
***********************************************************************/


#ifndef GAMEBOARD_H
#define GAMEBOARD_H


#include <string>


class ActionSpace
{
 public:

  ActionSpace(const std::string& name, bool discovered);
  const std::string name() const;
  const bool isTaken() const;
  bool isDiscovered() const;
  
  void take();
  void free();
  void updateForNextRound();
  void reset();
  
 private:
 
  std::string _name;
  bool _isDiscovered;
  bool _isTaken;
  
};



class GameBoard
{
 public:
  
  enum Board
  {
    TWO_PLAYERS,
    THREE_PLAYERS,
    FOUR_PLAYERS,
    FIVE_PLAYERS;
  };
  
  enum Action
  {
    OCCUP_ONE,
    OCCUP_TWO,
    START_PL,
    MAJ_MIN_IMPROV,
    DAY_LABORER,
    BUILD_ROOM,
    FAM_GROWTH,
    FAM_GROW_WR,
    RENOVATION,
    BUILD_FENCES,
    PLOW,
    SOW,
    PLOW_SOW,
    SOW_BAKE,
    ONE_GRAIN,
    ONE_VEGETABLE,
    FISH_POND,
    TRAV_ARTIST,
    ONE_BUILD_RES,
    REED_FOOD_STONE,
    THREE_WOOD,
    TWO_WOOD,
    ONE_CLAY,
    ONE_REED,
    ONE_STONE,
    ONE_SHEEP,
    ONE_BOAR,
    ONE_CATTLE,    
  };
  
 private:
  
};


#endif //GAMEBOARD_H
