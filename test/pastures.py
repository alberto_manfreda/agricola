import sys
from enum import Enum


class Direction(Enum):
    """
    """
    UP = 1
    RIGHT = 2
    DOWN = 3
    LEFT = 4

class SquareStatus(Enum):
    """
    """
    EMPTY = 1
    HOUSE = 2
    FIELD = 3
    PASTURE = 4
    STABLE = 5


class FarmSquare():
    """
    """
    EMPTY_CHAR = ' '
    FIELD_CHAR = 'F'
    HOUSE_CHAR = 'H'
    STABLE_CHAR = 'S'
    PASTURE_CHAR = 'P'
    HSIZE = 7
    VSIZE = 5
    
    def __init__(self, status):
        """
        """
        self.status = status
       
    def __str__(self):
        """
        """
        hmiddle = int(self.HSIZE / 2)
        vmiddle = int(self.VSIZE / 2)
        #Initialize to empty
        drawn_area = [[self.EMPTY_CHAR] * self.HSIZE\
                      for i in range(self.VSIZE)]
        # If the square is empty do nothing
        if (self.status == SquareStatus.EMPTY):
            pass
        # Stables go in the top-right corner
        elif (self.status == SquareStatus.STABLE):
            drawn_area[0][self.HSIZE - 1] = self.STABLE_CHAR
        # Houses and fields go in the middle
        elif (self.status == SquareStatus.FIELD):
            drawn_area[vmiddle][hmiddle] = self.FIELD_CHAR
        elif (self.status == SquareStatus.HOUSE):
            drawn_area[vmiddle][hmiddle] = self.HOUSE_CHAR
        elif (self.status == SquareStatus.PASTURE):
            drawn_area[vmiddle][hmiddle] = self.PASTURE_CHAR        
        else:
            raise AttributeError('Unknown status: %s' % self.status)
        return drawn_area
    
    def __repr__(self):
        """
        """
        return(str(self.status.name))

        
class Farm:
    """
    """
    def __init__(self, size=(5,3)):
        """
        """
        self.num_columns = size[0]
        self.num_rows = size[1]
        self.squares = [[FarmSquare(SquareStatus.EMPTY)] * self.num_rows\
                        for i in range(self.num_columns)]
        self.edges = self.num_edges() * [False]
                
    def square(self, col, row):
        """
        """
        return self.squares[col][row]
    
    def set_square_status(self, col, row, status):
        """
        """
        self.squares[col][row] = FarmSquare(status)
    
    def is_fenced(self, col, row, direction):
        """
        """
        return self.edges[self.edge_id(col, row, direction)]
    
    def size(self):
        """
        """
        return self.num_rows * self.num_columns
    
    def num_edges(self):
        """
        """
        return self.num_rows + self.num_columns + 2 * self.size()
    
    def edge_id(self, col, row, direction):
        """
        """
        mc = 2 * self.num_columns + 1
        if (direction == Direction.UP):
            return col + mc * row
        elif (direction == Direction.RIGHT):
            return col + mc * row + self.num_columns + 1
        elif (direction == Direction.DOWN):
            return col + mc * (row + 1)
        elif (direction == Direction.LEFT):
            return col + mc * row + self.num_columns
        else:
            raise RuntimeError('Invalid direction')
    
    def are_valid_coords(self, col, row):
        """
        """
        if (col >= 0) and (col < self.num_columns):
            if (row >= 0) and (row < self.num_rows):
                return True
        return False
            
    def __str__(self):
        """
        """
        GRID_CHAR = '.'
        VFENCE_CHAR = '+'
        HFENCE_CHAR = '+'
        num_col_chars = self.num_columns * (FarmSquare.HSIZE + 1) + 1
        num_row_chars = self.num_rows * (FarmSquare.VSIZE + 1) + 1
        draw_area = [[GRID_CHAR] * num_col_chars for i in range(num_row_chars)]
        edge_id = 0
        # Loop on all the rows        
        for row in range (self.num_rows):
            # Draw the horizontal edges on the upper side
            for col in range(self.num_columns):
                if self.edges[edge_id]:
                    for idx in range(0, FarmSquare.HSIZE):
                        i = 1 + col * (FarmSquare.HSIZE + 1) + idx
                        j = row * (FarmSquare.VSIZE + 1)
                        try:
                            draw_area[j][i] = HFENCE_CHAR
                        except IndexError as e:
                            print (j,i)
                            sys.exit(e)
                edge_id += 1
            #Draw the vertical fences on the sides of the squares
            for col in range(self.num_columns + 1):
                if self.edges[edge_id]:
                    for idx in range(0, FarmSquare.VSIZE):
                        i = col * (FarmSquare.HSIZE + 1)
                        j = 1 + row * (FarmSquare.VSIZE + 1) + idx
                        try:
                            draw_area[j][i] = VFENCE_CHAR
                        except IndexError as e:
                            print (j,i)
                            sys.exit(e)
                edge_id += 1
            # Draw the squares
            for col in range(self.num_columns):
                try:
                    square_area = self.squares[col][row].__str__()
                except IndexError as e:
                    print(col,row)
                    sys.exit(e)
                for idx in range(0, FarmSquare.HSIZE):
                    for idy in range(0, FarmSquare.VSIZE):
                        i = 1 + col * (FarmSquare.HSIZE + 1) + idx
                        j = 1 + row * (FarmSquare.VSIZE + 1) + idy
                        try:
                            draw_area[j][i] = square_area[idy][idx]
                        except IndexError as e:
                            print (e)
                            sys.exit(1)
        # Finally draw the edges on the bottom
        for col in range(self.num_columns):
            if self.edges[edge_id]:
                for idx in range(0, FarmSquare.HSIZE):
                    i = 1 + col * (FarmSquare.HSIZE + 1) + idx
                    j = self.num_rows * (FarmSquare.VSIZE + 1)
                    draw_area[j][i] = HFENCE_CHAR
            edge_id += 1
        return '\n'.join( [ ''.join(row) for row in draw_area] )



class PastureFinder:
    """
    """
    NOT_VISITED = -2
    NOT_PASTURE = -1
    
    def coordinate_step(self, col, row, direction):
        """
        """
        _col = col
        _row = row
        if (direction == Direction.UP):
            _row -= 1
        elif (direction == Direction.RIGHT):
            _col += 1
        elif (direction == Direction.DOWN):
            _row += 1
        elif (direction == Direction.LEFT):
            _col -= 1
        else:
            raise RuntimeError('Invalid direction')
        return _col, _row
    
    def pasture_allowed(self, col, row):
        """
        """
        _status = (self.farm.square(col, row)).status
        if not ((_status == SquareStatus.HOUSE) or \
               (_status == SquareStatus.FIELD)) :
            #print('Square is occupied. Stop building pasture...')
            return True
        else:
            return False
    
        
    def run(self, farm):
        """
        """
        self.num_pastures = 0
        self.farm = farm
        self.num_pastures = 0
        self._squares = [[self.NOT_VISITED] * self.farm.num_rows\
                         for i in range(self.farm.num_columns)]
        # This 2d list will hold the partial results of the algorithm. 
        # It has the same dimensions of the farm.squares list.
        # Meaning of the values:
        # -2 -> not checked yet
        # -1 -> not a pasture
        # any number >= 0 -> belong to the corresponding pasture
        for row in range (self.farm.num_rows):
            for col in range (self.farm.num_columns):
                #print('\n')
                #print('Square:', col, row)
                # If the square has been already visited skip it
                if (self._squares[col][row] != self.NOT_VISITED):
                    #print('Already visited.')
                    continue
                # Check whether the square is free for pasture
                if not (self.pasture_allowed(col, row)):
                    self._squares[col][row] = self.NOT_PASTURE
                    continue
                # Start trying to build a pasture form this square
                current_pasture  = [(col, row)]
                if self.expand_pasture(current_pasture, col, row):
                    #print('Pasture found (n. %d)! Squares:' %\
                    #      self.num_pastures)
                    for (col, row) in current_pasture:
                        #print (col, row)
                        self._squares[col][row] = self.num_pastures
                    self.num_pastures += 1
        #print(self._squares)
        # Now assign the status to the farm squares
        for row in range (self.farm.num_rows):
            for col in range (self.farm.num_columns):
                if (self._squares[col][row] >= 0):
                    self.farm.set_square_status(col, row, SquareStatus.PASTURE)
        return self.num_pastures
    
    def expand_pasture(self, current_pasture, col, row):
        """ Expand a pasture starting from the given square. Return the
            success status.            
        """
        #print ('Expanding pasture %d from square %d %d' %(self.num_pastures,
        #                                                  col, row))
        success = False
        for direction in Direction:
            # If there is a fence in that direction do nothing and check 
            # the next direction
            if self.farm.is_fenced(col, row, direction) == True:
                #print('Fenced in direction % s' % direction.name)
                #print('edge_id is %d' % edge_id)
                continue
            # If the passage is open in that direction (no fence):
            else:
                # 1) Get the coordinates of the adjacent square
                nb_col, nb_row = self.coordinate_step(col, row, direction)
                #print('Checking neighbor %d %d' % (nb_col, nb_row))
                # 2) Check that they are valid, i.e. that they are inside
                #    the farm
                if not (self.farm.are_valid_coords(nb_col, nb_row)):
                    #print('Out of grid. Stop building pasture')
                    for col, row in current_pasture:
                        self._squares[col][row] = self.NOT_PASTURE
                    return False
                # 3) If the square is already counted in the pasture, do
                #    nothing
                if (nb_col, nb_row) in current_pasture:
                   #print('Already in this pasture. Going on...')
                   continue
                # 4) Check  that the square is not already occupied
                #    by a field or a house and that it has not already
                #    been marked as NOT_PASTURE
                if not (self.pasture_allowed(nb_col, nb_row)):
                    for col, row in current_pasture:
                        self._squares[col][row] = self.NOT_PASTURE
                    return False
                if (self._squares[nb_col][nb_row] == self.NOT_PASTURE):
                    #print('Connceted to a bad square. Stop building pasture.')
                    for col, row in current_pasture:
                        self._squares[col][row] = self.NOT_PASTURE
                    return False
                # 5) If everything is ok, add the square to the
                # current pasture
                current_pasture.append((nb_col, nb_row))
                # 6) Keep expanding he pasture from it. Check that it
                #    goes well, if not return False. That will cause
                #    a recursive exit from the cycle.
                if not self.expand_pasture(current_pasture,
                                           nb_col, nb_row):
                    return False
        return True
            

if __name__=='__main__':
    """
    """
    farm = Farm((5,3))
    farm.squares[0][1] = FarmSquare(SquareStatus.FIELD)
    
    farm.squares[1][1] = FarmSquare(SquareStatus.HOUSE)
    farm.edges[12] = True
    farm.edges[17] = True
    farm.edges[23] = True
    
    
    farm.edges[3] = True
    farm.edges[4] = True
    farm.edges[8] = True
    farm.edges[10] = True
    farm.edges[15] = True
    farm.edges[19] = True
    farm.edges[20] = True
    farm.edges[25] = True
    
    farm.edges[13] = True
    farm.edges[18] = True
    farm.edges[24] = True
    
    finder = PastureFinder()
    num_pastures = finder.run(farm)
    print('\n')
    print(farm)
    print('\n')

    print('This farm has %d pastures.' % num_pastures)   

