DEPS = gui game

QT += widgets
QT += svg

equals(QT_MAJOR_VERSION, 5) {
  CONFIG += c++11
  for(dep, DEPS): \
     VPATH += $$(AGRIROOT)/$${dep}/src $$(AGRIROOT)/$${dep}/include
}

equals(QT_MAJOR_VERSION, 4) {
  QMAKE_CXXFLAGS += -std=c++11
  for(dep, DEPS): \
     VPATH += $$(AGRIROOT)/$${dep}/src $$(AGRIROOT)/$${dep}/include
  }

INCLUDEPATH += $$(AGRIROOT)
OBJECTS_DIR = $$(AGRIROOT)/build
MOC_DIR = $$(AGRIROOT)/moc
DESTDIR = $$(AGRIROOT)/bin

HEADERS += goods.h \
           player.h \
           engine.h

SOURCES += goods.cpp \
           player.cpp \
           engine.cpp \
           src/agricola.cpp

INCLUDEPATH +=
LIBS +=
