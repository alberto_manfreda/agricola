/***********************************************************************
Copyright (C) 2018 Alberto Manfreda

For the license terms see the file LICENSE, distributed along with this
software.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
***********************************************************************/

#include<string>
#include <iostream>

#include <QApplication>
#include <QMainWindow>

#include "game/include/engine.h"

// Definition of the application name
const std::string __programName__ = "Agricola";

// Main function
int main(int argc, char *argv[])
{
  QApplication app(argc, argv);
  //QMainWindow window;
  //window.setWindowTitle(__programName__.c_str());
  //window.show();
  GameEngine engine;
  engine.addPlayer(PlayerAttr::Color::RED, "Paolo");
  //engine.addPlayer(PlayerAttr::Color::BLUE, "Mario");
  //engine.addPlayer(PlayerAttr::Color::WHITE, "Giada");
  engine.run();
  //return app.exec();
  return 0;
}
