#!/bin/bash

# See this stackoverflow question
# http://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within
# for the magic in this command
SETUP_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#
# Base package root. All the other releavant folders are relative to this
# location.
#
export AGRIROOT=$SETUP_DIR
echo "AGRIROOT set to " $AGRIROOT


#
# Add the bin folder to the $PATH environmental variable.
#
export PATH=$AGRIROOT/bin:$PATH
